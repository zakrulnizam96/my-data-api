<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKesJenayahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kes_mahkamah', function (Blueprint $table) {
            $table->id();
            $table->integer('type')->nullable();
            $table->float('year')->nullable();
            $table->float('count')->nullable();
            $table->float('category')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kes_mahkamah');
    }
}
