<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unemployment;

class UnemploymentController extends Controller
{
    public function all()
    {
        $data = Unemployment::all()->groupBy('state');
        return response()->json($data, 200);
    }

    public function getById($id)
    {
        $data = Unemployment::findOrFail($id);
        return response()->json($data, 200);
    }

    public function getDataByState($state)
    {
        $data = Unemployment::where("state", $state)->get();
        return response()->json($data, 200);
    }

    public function getDataByYear($year)
    {
        $data = Unemployment::where("year", $year)->get();
        return response()->json($data, 200);
    }
}
