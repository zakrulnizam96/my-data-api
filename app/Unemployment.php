<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unemployment extends Model
{
    protected $table = 'unemployment';
    protected $primaryKey = 'id';
}
