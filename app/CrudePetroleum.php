<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrudePetroleum extends Model
{
    protected $table = 'crude_petroleum';
    protected $primaryKey = 'id';
}
