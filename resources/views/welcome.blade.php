 <!DOCTYPE html>
 <html class="no-js" lang="en">

 <head>
     <meta charset="utf-8">
     <link rel="canonical" href="http://my-data.tk/" />
     <link rel="icon" href="https://www.countryflags.com/wp-content/uploads/malaysia-flag-png-xl.png">
     <!-- Primary Meta Tags -->
     <title>Malaysia General Statistic Data Set | my-data.tk</title>
     <meta name="title" content="Malaysia General Statistic Data Set | my-data.tk">
     <meta name="description"
         content="Open source malaysia real data set API that can be use without any limit request!">

     <!-- Open Graph / Facebook -->
     <meta property="og:type" content="website">
     <meta property="og:url" content="http://my-data.tk/">
     <meta property="og:title" content="Malaysia General Statistic Data Set | my-data.tk">
     <meta property="og:description"
         content="Open source malaysia real data set API that can be use without any limit request!">
     <meta property="og:image" content="http://my-data.tk/images/logomdtk.jpg">

     <!-- Twitter -->
     <meta property="twitter:card" content="summary_large_image">
     <meta property="twitter:url" content="http://my-data.tk/">
     <meta property="twitter:title" content="Malaysia General Statistic Data Set | my-data.tk">
     <meta property="twitter:description"
         content="Open source malaysia real data set API that can be use without any limit request!">
     <meta property="twitter:image" content="http://my-data.tk/images/logomdtk.jpg">

     <meta http-equiv="cleartype" content="on">
     <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">

     <link rel="stylesheet" href="css/hightlightjs-dark.css">
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
         integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
         crossorigin="anonymous"></script>
     <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.8.0/highlight.min.js"></script>
     <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,500|Source+Code+Pro:300" rel="stylesheet">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
         integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
     <link rel="stylesheet" href="css/style.css" media="all">
     <script>
         hljs.initHighlightingOnLoad();

     </script>

     <!-- Global site tag (gtag.js) - Google Analytics -->
     <script async src="https://www.googletagmanager.com/gtag/js?id=G-FTEK0WRXHN"></script>
     <script>
         window.dataLayer = window.dataLayer || [];

         function gtag() {
             dataLayer.push(arguments);
         }
         gtag('js', new Date());

         gtag('config', 'G-FTEK0WRXHN');

     </script>
 </head>

 <body class="one-content-column-version">
     <div class="left-menu">
         <div class="content-logo">
             <h1><img alt="platform by Emily van den Heever from the Noun Project"
                     title="platform by Emily van den Heever from the Noun Project"
                     src="https://www.countryflags.com/wp-content/uploads/malaysia-flag-png-xl.png" width="auto"
                     height="16" />
                 <span>MY Data API</span><br>
                 <i style="font-size: smaller">by ZakrulNizam</i>
             </h1>
         </div>
         <div class="content-menu">
             <ul>
                 <li class="scroll-to-link active" data-target="get-started">
                     <a>Get Started</a>
                 </li>
                 <li class="scroll-to-link" data-target="get-unemployment">
                     <a> Unemployment Data</a>
                 </li>
                 <li class="scroll-to-link" data-target="get-oku">
                     <a>Disable Person (OKU)</a>
                 </li>
                 <li class="scroll-to-link" data-target="get-crude">
                     <a>Crude Petrol</a>
                 </li>
                 <li class="scroll-to-link" data-target="get-court">
                     <a>Settled Court Case</a>
                 </li>
                 <li class="scroll-to-link">
                     <a style="font-size: smaller"> More coming soon...</a>
                 </li>
             </ul>
         </div>
     </div>


     <div class="content-page">
         <div class="content">
             <div class="overflow-hidden content-section" id="content-get-started">
                 <h2 id="get-started">Get started</h2>
                 <p>
                     My-data.tk API provides programmatic access to read Malaysia Gov data set for analytical
                     purposes.<br>Retrieve an easy json data without using access token and unlimited API call.
                 </p>
                 <p>
                     If you have any problem while using this <strong>API</strong>, please contact us at <a
                         href="mailto:zakrulnizam96@gmail.com">zakrulnizam96@gmail.com</a> for further assistance.
                 </p>
                 <p>This is non-for-profit project, if you want to contribute for this project, kindly contact me.</p>
             </div>

             <div class="overflow-hidden content-section" id="unemployment">
                 <h2 id="get-unemployment">Unemployment Malaysia Data Set (1982 - 2016)</h2>

                 <h3>Get all data</h3>
                 <p>
                     To get all unemployment data, you need to make a GET call to the following url :<br>
                     <code class="higlighted">http://@php echo $_SERVER['HTTP_HOST'] @endphp/api/unemployment</code>
                 </p>

                 <h3>Get data by ID</h3>
                 <p>
                     To get unemployment data by ID, you need to make a GET call to the following url :<br>
                     <code class="higlighted">http://@php echo $_SERVER['HTTP_HOST'] @endphp/api/unemployment/{id}</code>
                 </p>

                 <h3>Get data by year</h3>
                 <p>
                     To get all unemployment data by year, you need to make a GET call to the following url :<br>
                     <code class="higlighted">http://@php echo $_SERVER['HTTP_HOST'] @endphp/api/unemployment/year/{query_parameter}</code>
                 </p>
                 <br>
                 <h4>QUERY PARAMETERS</h4>
                 <table>
                     <thead>
                         <tr>
                             <th>Field</th>
                             <th>Type</th>
                             <th>Description</th>
                         </tr>
                     </thead>
                     <tbody>
                         <tr>
                             <td>year</td>
                             <td>String</td>
                             <td>any year between 1982 - 2016</td>
                         </tr>
                     </tbody>
                 </table>

                 <h3>Get data by state</h3>
                 <p>
                     To get all unemployment data by state, you need to make a GET call to the following url :<br>
                     <code class="higlighted">http://@php echo $_SERVER['HTTP_HOST'] @endphp/api/unemployment/state/{query_parameter}</code>
                 </p>
                 <br>
                 <h4>QUERY PARAMETERS</h4>
                 <table>
                     <thead>
                         <tr>
                             <th>Field</th>
                             <th>Type</th>
                             <th>Description</th>
                         </tr>
                     </thead>
                     <tbody>
                         <tr>
                             <td>State</td>
                             <td>String</td>
                             <td>
                                 <ul style="color:white">
                                     <li>Johor</li>
                                     <li> Kedah</li>
                                     <li>Kelantan</li>
                                     <li>Malaysia</li>
                                     <li>Melaka</li>
                                     <li>Negeri Sembilan</li>
                                     <li>Pahang</li>
                                     <li>Perak</li>
                                     <li>Perlis</li>
                                     <li>Pulau Pinang</li>
                                     <li>Sabah</li>
                                     <li>Sarawak</li>
                                     <li>Selangor</li>
                                     <li>Terengganu</li>
                                     <li>W.P Labuan</li>
                                     <li>W.P. Kuala Lumpur</li>
                                     <li>W.P.Putrajaya</li>
                                 </ul>
                             </td>
                         </tr>
                     </tbody>
                 </table>
             </div>

             <div class="overflow-hidden content-section" id="oku">
                 <h2 id="get-oku">Disable Person (OKU) data (2018)</h2>

                 <h3>Get all data</h3>
                 <p>
                     To get all disable person data, you need to make a GET call to the following url :<br>
                     <code class="higlighted">http://@php echo $_SERVER['HTTP_HOST'] @endphp/api/disable-person</code>
                 </p>

                 <h3>Get data by ID</h3>
                 <p>
                     To get disable person data by ID, you need to make a GET call to the following url :<br>
                     <code class="higlighted">http://@php echo $_SERVER['HTTP_HOST'] @endphp/api/disable-person/{id}</code>
                 </p>

                 <h3>Get data by state</h3>
                 <p>
                     To get all data by state, you need to make a GET call to the following url :<br>
                     <code class="higlighted">http://@php echo $_SERVER['HTTP_HOST'] @endphp/api/disable-person/state/{query_parameter}</code>
                 </p>
                 <br>
                 <h4>QUERY PARAMETERS</h4>
                 <table>
                     <thead>
                         <tr>
                             <th>Field</th>
                             <th>Type</th>
                             <th>Description</th>
                         </tr>
                     </thead>
                     <tbody>
                         <tr>
                             <td>State</td>
                             <td>String</td>
                             <td>
                                 <ul style="color:white">
                                     <li>Johor</li>
                                     <li> Kedah</li>
                                     <li>Kelantan</li>
                                     <li>Malaysia</li>
                                     <li>Melaka</li>
                                     <li>Negeri Sembilan</li>
                                     <li>Pahang</li>
                                     <li>Perak</li>
                                     <li>Perlis</li>
                                     <li>Pulau Pinang</li>
                                     <li>Sabah</li>
                                     <li>Sarawak</li>
                                     <li>Selangor</li>
                                     <li>Terengganu</li>
                                     <li>WP Labuan</li>
                                     <li>WP Kuala Lumpur dan Putrajaya</li>
                                 </ul>
                             </td>
                         </tr>
                     </tbody>
                 </table>

                 <h3>Get data by race/ethnic</h3>
                 <p>
                     To get all data by ethnic, you need to make a GET call to the following url :<br>
                     <code class="higlighted">http://@php echo $_SERVER['HTTP_HOST'] @endphp/api/disable-person/race/{query_parameter}</code>
                 </p>
                 <br>
                 <h4>QUERY PARAMETERS</h4>
                 <table>
                     <thead>
                         <tr>
                             <th>Field</th>
                             <th>Type</th>
                             <th>Description</th>
                         </tr>
                     </thead>
                     <tbody>
                         <tr>
                             <td>Race</td>
                             <td>String</td>
                             <td>
                                 <ul style="color:white">
                                     <li>Melayu</li>
                                     <li>Cina</li>
                                     <li>India</li>
                                     <li>Pribumi Semenanjung</li>
                                     <li>Pribumi Sabah</li>
                                     <li>Pribumi Sarawak</li>
                                     <li>Lain-lain</li>
                                 </ul>
                             </td>
                         </tr>
                     </tbody>
                 </table>
             </div>

             <div class="overflow-hidden content-section" id="crude">
                 <h2 id="get-crude">Import Export Crude Petroleum In Malaysia</h2>

                 <h3>Get all data</h3>
                 <p>
                     To get all data, you need to make a GET call to the following url :<br>
                     <code class="higlighted">http://@php echo $_SERVER['HTTP_HOST'] @endphp/api/kes-mahkamah</code>
                 </p>

                 <h3>Get data by ID</h3>
                 <p>
                     To get data by ID, you need to make a GET call to the following url :<br>
                     <code class="higlighted">http://@php echo $_SERVER['HTTP_HOST'] @endphp/api/kes-mahkamah/{id}</code>
                 </p>

                 <h3>Get data by year</h3>
                 <p>
                     To get all data by year, you need to make a GET call to the following url :<br>
                     <code class="higlighted">http://@php echo $_SERVER['HTTP_HOST'] @endphp/api/kes-mahkamah/year/{query_parameter}</code>
                 </p>
                 <br>
                 <h4>QUERY PARAMETERS</h4>
                 <table>
                     <thead>
                         <tr>
                             <th>Field</th>
                             <th>Type</th>
                             <th>Description</th>
                         </tr>
                     </thead>
                     <tbody>
                         <tr>
                             <td>year</td>
                             <td>String</td>
                             <td>1963 - 2016</td>
                         </tr>
                     </tbody>
                 </table>
             </div>

             <div class="overflow-hidden content-section" id="court">
                 <h2 id="get-court">Settled Court Case</h2>

                 <h3>Get all data</h3>
                 <p>
                     To get all data, you need to make a GET call to the following url :<br>
                     <code class="higlighted">http://@php echo $_SERVER['HTTP_HOST'] @endphp/api/kes-mahkamah</code>
                 </p>

                 <h3>Get data by ID</h3>
                 <p>
                     To get data by ID, you need to make a GET call to the following url :<br>
                     <code class="higlighted">http://@php echo $_SERVER['HTTP_HOST'] @endphp/api/kes-mahkamah/{id}</code>
                 </p>

                 <h3>Get data by year</h3>
                 <p>
                     To get all data by year, you need to make a GET call to the following url :<br>
                     <code class="higlighted">http://@php echo $_SERVER['HTTP_HOST'] @endphp/api/kes-mahkamah/year/{query_parameter}</code>
                 </p>
                 <br>
                 <h4>QUERY PARAMETERS</h4>
                 <table>
                     <thead>
                         <tr>
                             <th>Field</th>
                             <th>Type</th>
                             <th>Description</th>
                         </tr>
                     </thead>
                     <tbody>
                         <tr>
                             <td>year</td>
                             <td>String</td>
                             <td>2009 - 2018</td>
                         </tr>
                     </tbody>
                 </table>

                 <h3>Get data by case category</h3>
                 <p>
                     To get all data by year, you need to make a GET call to the following url :<br>
                     <code
                         class="higlighted">http://@php echo $_SERVER['HTTP_HOST'] @endphp/api/kes-mahkamah/category/{query_parameter}</code>
                 </p>
                 <br>
                 <h4>QUERY PARAMETERS</h4>
                 <table>
                     <thead>
                         <tr>
                             <th>Field</th>
                             <th>Type</th>
                             <th>Description</th>
                         </tr>
                     </thead>
                     <tbody>
                         <tr>
                             <td>category</td>
                             <td>String</td>
                             <td>Kes Pembuangan kerja / Kes-kes Bukan Pembuangan Kerja</td>
                         </tr>
                     </tbody>
                 </table>

             </div>

             <div>
                 <h3>Google Line Chart Demo with my-data API endpoint</h3>

                 <div id="chart" style="width:auto; height:300px;"></div>
             </div>
         </div>
     </div>
     <a class="d-none d-xl-block" href="https://gitlab.com/zakrulnizam96/my-data-api" target="_blank">
         <span
             style="font-family: tahoma; font-size: 20px; position:fixed; top:50px; right:-45px; display:block; -webkit-transform: rotate(45deg); -moz-transform: rotate(45deg); background-color:red; color:white; padding: 4px 30px 4px 30px; z-index:99">Fork
             Me On GitLab</span>
     </a>

     <script src="js/script.js"></script>
     <script>
         google.charts.load('visualization', {
             packages: ['corechart']
         });
         google.charts.setOnLoadCallback(drawLineChart);

         function drawLineChart() {
             $.ajax({
                 url: "/api/unemployment/state/Johor",
                 dataType: "json",
                 type: "GET",
                 success: function(data) {
                     var arrStats = [
                         ['Year', 'Percentage']
                     ]; // Define an array and assign columns for the chart.

                     data.reverse();
                     // Loop through each data and populate the array.
                     $.each(data, function(index, value) {
                         arrStats.push([value.year.toString(), value.percentage]);
                     });

                     console.log(arrStats);
                     // Set chart Options.
                     var options = {
                         title: 'Unemployment In Johor Malaysia',
                         curveType: 'function',
                         legend: {
                             position: 'bottom',
                             textStyle: {
                                 color: '#555',
                                 fontSize: 14
                             }
                         } // You can position the legend on 'top' or at the 'bottom'.
                     };

                     // Create DataTable and add the array to it.
                     var figures = google.visualization.arrayToDataTable(arrStats)

                     // Define the chart type (LineChart) and the container (a DIV in our case).
                     var chart = new google.visualization.LineChart(document.getElementById('chart'));
                     chart.draw(figures, options); // Draw the chart with Options.
                 },
                 error: function(xhr, textStatus, errorThrown) {
                     console.log('error');
                 }
             });
         }

     </script>
 </body>

 </html>
