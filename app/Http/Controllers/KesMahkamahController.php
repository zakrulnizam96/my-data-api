<?php

namespace App\Http\Controllers;
use App\KesMahkamah;
use Illuminate\Http\Request;

class KesMahkamahController extends Controller
{
    public function all(){
        $data = KesMahkamah::all()->groupBy('year');
        return response()->json($data, 200);
    }

    public function getById($id)
    {
        $data = KesMahkamah::findOrFail($id);
        return response()->json($data, 200);
    }

    public function getDataByState($state)
    {
        $data = KesMahkamah::where("state", $state)->get();
        return response()->json($data, 200);
    }

    public function getDataByYear($year)
    {
        $data = KesMahkamah::where("year", $year)->get();
        return response()->json($data, 200);
    }

    public function getDataByCategory($category)
    {
        $data = KesMahkamah::where("category", $category)->get();
        return response()->json($data, 200);
    }
}
