<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DisabledPerson extends Model
{
    protected $table = 'disable_person';
    protected $primaryKey = 'id';
}
