<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CrudePetroleum;

class CrudeController extends Controller
{
    public function all()
    {
        $data = CrudePetroleum::all();
        return response()->json($data, 200);
    }

    public function getById($id){
        $data = CrudePetroleum::findOrFail($id);
        return response()->json($data, 200);
    }

    public function getDataByYear($year)
    {
        $data = CrudePetroleum::where("year", $year)->get();
        return response()->json($data, 200);
    }
}
